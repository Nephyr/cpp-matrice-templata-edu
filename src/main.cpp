#include "matrix.h"
#include <cstdlib>      // rand
#include <ctime>        // time
#include <algorithm>    // replace_if

//=============================================================================
//      MATRIX - FUNZIONI DI SUPPORTO
//=============================================================================

template <typename IterT>
void set_random(IterT b, IterT e) 
{
    std::srand (std::time(NULL));
    
    for(; b != e; ++b)
        *b = rand() % 100;
}

struct is_2_divs
{
    bool operator()(int value) const 
    {
        return value % 2 != 0;
    }
};

struct add_100 
{
    int operator()(int value) const
    {
        return 100 + value;
    }
};

struct DBEntry 
{
    std::string title;
    int vote;
    
    DBEntry() : title(""), vote(0) {}
    DBEntry(const std::string &t, int v=0) : title(t), vote(v) {}
};

//=============================================================================
//      MATRIX - FUNZIONI DI TEST PER CLASSE MATRICE
//=============================================================================

void test_operator()
{
    Matrice<int>    mtr1 = Matrice<int>();
    Matrice<char>   mtr2(3, 3, '#');
    Matrice<int>    mtr3(3, 3, 125);
    Matrice<char>   mtr4(mtr3);

    mtr1 = mtr2;
    mtr1 = mtr3;

    mtr1(1, 2) = 200;
    mtr2(1, 2) = mtr1(1, 2);

    std::cout << mtr1 << std::endl;
    std::cout << mtr2 << std::endl;
    std::cout << mtr3 << std::endl;
    std::cout << mtr4 << std::endl;
}

void test_show()
{
    Matrice<int>    mtr1 = Matrice<int>();
    Matrice<const char*>   mtr2(3, 3, "ciao");
    Matrice<int>    mtr3(3, 3, 125);
    Matrice<char>   mtr4(mtr3);

    mtr1.show();
    mtr2.show();
    mtr3.show();
    mtr4.show();
}

void test_resize()
{
    Matrice<int>    mtr1 = Matrice<int>();
    Matrice<char>   mtr2(2, 2, '#');

    std::cout << mtr1 << std::endl;
    std::cout << mtr2 << std::endl;
    std::cout << std::endl;

    mtr1.resize(4, 8);
    mtr2.resize(2, 3);

    std::cout << mtr1 << std::endl;
    std::cout << mtr2 << std::endl;
    std::cout << std::endl;

    mtr1(1, 1) = 42;
    mtr2(1, 2) = mtr1(1, 1);

    mtr1.show();
    mtr2.show();

    mtr1.resize(1, 1, 10);
    mtr2.resize(5, 4, '@');
    
    std::cout << mtr1 << std::endl;
    std::cout << mtr2 << std::endl;
}

void test_functor()
{
    Matrice<int>    mtr1(3, 3, 100);
    Matrice<char>   mtr2(3, 3, '#');
    Matrice<char>   mtr3(mtr2);
    Matrice<int>    mtr4(mtr1);

    if(verify(mtr1, mtr4, checkSame<int>()))
        std::cout << "TEST FUNTORE: Le matrici sono uguali!" << std::endl;
    else
        std::cout << "TEST FUNTORE: Le matrici NON sono uguali!" << std::endl;

    if(verify(mtr2, mtr3, checkSame<char>()))
        std::cout << "TEST FUNTORE: Le matrici sono uguali!" << std::endl;
    else
        std::cout << "TEST FUNTORE: Le matrici NON sono uguali!" << std::endl;
}

void test_iterators(void) 
{
    Matrice<int>    mtr1(3, 3, 100);
    Matrice<char>   mtr2(3, 3, '#');
    
    set_random(mtr1.begin(), mtr1.end());
    std::cout << mtr1 << std::endl;

    Matrice<int>::const_iterator    itr1;
    Matrice<char>::const_iterator   itr2;
    
    std::cout << "[ ";
    for(itr1 = mtr1.begin(); itr1 != mtr1.end(); itr1++)
        std::cout << (((*itr1) % 8 == 0) ? "true" : "false");
    std::cout << " ]" << std::endl;

    std::cout << "[ ";
    for(itr2 = mtr2.begin(); itr2 != mtr2.end(); itr2++)
        std::cout << (((*itr2) == '#') ? "true" : "false");
    std::cout << " ]" << std::endl;
        
    std::replace_if(mtr1.begin(), mtr1.end(), is_2_divs(), 0);
    std::cout << mtr1 << std::endl;

    const Matrice<int> &mtr3 = mtr1;
    Matrice<int> mtr4(10, 2, 0);
    std::transform(mtr3.begin(), mtr3.end(), mtr4.begin(), add_100());
    std::cout << mtr4 << std::endl;

    Matrice<DBEntry> db(2, 2);
    db.setValue(1, 1, DBEntry("Chocabeck", 5));
    db.setValue(0, 0, DBEntry("Work in Progress", 2));
    db.begin()->vote = 10;

    std::cout << db.getValue(0, 0).title << "( " << db.getValue(0, 0).vote << " )" << std::endl;
    std::cout << db.getValue(1, 1).title << "( " << db.getValue(1, 1).vote << " )" << std::endl;
}

void test_constructor()
{
    Matrice<int>    mtr1 = Matrice<int>();
    Matrice<char>   mtr2(3, 3, '#');
    Matrice<int>    mtr3(3, 3, 125);
    Matrice<char>   mtr4(mtr3);
}

//=============================================================================
//      MATRIX - MAIN DEL PROGRAMMA
//=============================================================================

int main() 
{
    //-------------------------------------------------------------------------
    // TEST COSTRUTTORI
    //-------------------------------------------------------------------------

    std::cout << std::endl
              << "test_constructor()" 
              << std::endl
              << "{" 
              << std::endl;
    
    test_constructor();

    std::cout << "}" 
              << std::endl
              << "// END TEST"
              << std::endl;

    //-------------------------------------------------------------------------
    // TEST OPERATORI
    //-------------------------------------------------------------------------

    std::cout << std::endl
              << "test_operator()" 
              << std::endl
              << "{" 
              << std::endl;
    
    test_operator();

    std::cout << "}" 
              << std::endl
              << "// END TEST"
              << std::endl;

    //-------------------------------------------------------------------------
    // TEST STAMPA VALORI
    //-------------------------------------------------------------------------

    std::cout << std::endl
              << "test_show()" 
              << std::endl
              << "{" 
              << std::endl;
    
    test_show();

    std::cout << "}" 
              << std::endl
              << "// END TEST"
              << std::endl;

    //-------------------------------------------------------------------------
    // TEST RESIZE
    //-------------------------------------------------------------------------

    std::cout << std::endl
              << "test_resize()" 
              << std::endl
              << "{" 
              << std::endl;
    
    test_resize();

    std::cout << "}" 
              << std::endl
              << "// END TEST"
              << std::endl;

    //-------------------------------------------------------------------------
    // TEST FUNTORI
    //-------------------------------------------------------------------------

    std::cout << std::endl
              << "test_functor()" 
              << std::endl
              << "{" 
              << std::endl;
    
    test_functor();

    std::cout << "}" 
              << std::endl
              << "// END TEST"
              << std::endl;

    //-------------------------------------------------------------------------
    // TEST ITERATORI
    //-------------------------------------------------------------------------

    std::cout << std::endl
              << "test_iterators()" 
              << std::endl
              << "{" 
              << std::endl;
    
    test_iterators();

    std::cout << "}" 
              << std::endl
              << "// END TEST"
              << std::endl;

    //-------------------------------------------------------------------------

    std::cout << std::endl;
}
